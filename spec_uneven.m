function [Pxx  f v]=spec_uneven(xdata,xtim,nsp,Fs,varargin)
% Required
%   xdata / xtim: data and time vectors in days (function doesn't deal with
%               matrices)
%   nsp=number of block avg to apply hanning window
%Optional:
%    Fs: specify if both data sets were sampled evenly but are gappy
%       This is what will be used as default nyquist f rather than one
%       estimated from the gappy data due to masking of bad data
%    set Fs=0 if sampled unevenly
% Outputs:
%       Pxx and f, non-dimensionalized to Hz
% Refs: Shultz and Stattegger 1997 "Spectral analysis of unevenly spaced data
%       sets". 
% Improvements:

%% Options
tplot=1;
dfb=0;
davb=0;
if ~isempty(varargin)
    for c=1:floor(length(varargin)/2)
        switch varargin{2*c-1}
            case 'fband avg'
                dfb=varargin{c*2};
                if issorted(dfb)==0
                    error('Must supply frequencies in ascending order');
                end
            case 'n bands'
                davb=varargin{c*2};  
                if davb~=0
                    if isequal(davb,round(davb))==0 || issorted(davb)==0 || all(rem(davb,2))==0
                        error('Must supply n bands as integer odd values in ascending order');
                    end 
                end
            case 'tplot'
                tplot=varargin{c*2};
        end
    end
end

    
%% Removal of NaNs data points
gdpts=find(~isnan(xdata));
xdata=xdata(gdpts);
xtim=xtim(gdpts);
xdata=detrend(xdata);%-mean(xdata);


%% Checks

Nx=length(xdata); 
if length(xtim)~=Nx
    error('both xtim and xdata should have same dimensions');
end


%% Computations
dtx=mean(diff(xtim))*24*3600;
Tx=(xtim(end)-xtim(1))*24*3600;
if nsp>1
    Nsx=floor(2*Nx./(nsp+1)); % for segment, equals Nx if nsp=1
else
    Nsx=Nx;
end
dfx=1./(Nsx*dtx);% fundamental (lowest) freq 1/T 

%% Computatuins
df=dfx; % Eq 8 SS 1997
if Fs==0
   % relevant is truly using unevenly sampled data (and not evenly smapled with "bad" discarded data"
    dt=dtx; % Eq. 7 SS 1997
    Fnx=1./(2*dtx); % Avg nyquist
else
    Fnx=Fs/2; % nyquist
    dt=1./Fs;
end

tau0=0; %(1./Nx)*sum(xtim)-(1./Ny)*sum(ytim); %Eq 13. SS 1997 (if Fs was speicified, and data sets aligned, this should be 0


%% Lomb-scargle dft estimate for each ts
%Fsxy=2*Fnxy;% sampling freq
pc=0.95; % conf interval
f = transpose(df:df:Fnx); 
w=2*pi*f;
   
[X1 G ]=lsft(w,xdata,xtim*24*3600,tau0,'nblocks',nsp);
disp(Nsx*df)
Pxx=G./(Nsx*df);%(2*Fnx);%;

if nsp>1
    [CL v] = CL_cb(pc,nsp,'window','hanning');
else
    [CL v] = CL_cb(pc,nsp); 
end

%% Band avg
if davb~=0
    [Pxx f CL v]= bandavg(Pxx,f,davb,dfb,CL,v,nsp,0,'hanning');
end




%% Plot
if tplot==1
    colsym={[0 0 0],[0 0.8 0.4]};
    figure
    ax(1)=subplot(1,1,1);
 
    loglog(f,Pxx);hold on;
    yli=get(gca,'ylim');
    CLp=CL*5*yli(1);
    if length(CL(:,1))>1
        ii=2;
    else
        ii=1;
    end
    
    if all([any(CLp(ii:end,1)~=CLp(ii,1)) davb])
       plot(f,CLp(:,[1;3]),'k-');
       text(f(2),CLp(1,3),'95%','verticalalignment','bottom','fontsize',8);
    else
        errorbar(f(2),CLp(ii,2),CLp(ii,1)-CLp(ii,2),CLp(ii,2)-CLp(ii,3),'ko','markersize',2);
        text(1.1*f(2),CLp(ii,2),'95%','verticalalignment','bottom','fontsize',8);
    end
    

title([int2str(nsp) ' segments overlapping at 50% (hanning)-uneven' ]);
  xlabel('Frequency [Hz]');
  ylabel('PSD');
end