# Spectral Analysis MATLAB toolbox

Compute spectral estimates on even and unevenly (gappy) spaced data. 

Features
--------

- Spectral, cross-spectral an rotary analysis of evenly and unevenly spaced data.
- Least-square fourier transform (`lsft.m`) is used to compute the FFT for unevenly spaced data.
- Spectral confidence intervals and degrees of freedom are also provided for different windowing function


## Code Dependencies

- Requires statistical and signal processing toolboxes in MatLab.
- Eventually the uneven and evenly spaced main functions should be combined, as they differ mostly by how the sampling frequency and FFT is computed.


## Contribute

Eventually I'll include a demo, which users should run to test for bugs **prior** to updating (pushing) the code.

- [Issue Tracker](https://bitbucket.org/uwaoceandynamics/spectralanalysis/issues/)
- [Source Code](https://bitbucket.org/uwaoceandynamics/spectralanalysis/src)
---

*Author*: Cynthia Bluteau

*Date*: Oct 2016

*Institution*: University of Western Australia