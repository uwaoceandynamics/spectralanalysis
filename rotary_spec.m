function [r ephi fxy ax]=rotary_spec(u1,v1,Fs,tplot,varargin)
% Hasnt been thoroughly used or tested by CBluteau.
% Trying to learn how to do rotary spectra between 2 timeseries
% Starting first with one timeseries
% Does the copectra and spectra should be done with the same averaging
%% Options
nsp=16;
 ig_nan=0;
 
if ~isempty(varargin)
    for c=1:floor(length(varargin)/2)
        switch varargin{2*c-1}
            case 'nblocks'
                nsp=varargin{c*2};
%             case 'fbands'
%                 dfb=varargin{c*2};
%                 if issorted(dfb)==0
%                     error('Must supply frequencies in ascending order');
%                 end
%             case 'nbands'
%                 davb=varargin{c*2};  
%                 if davb~=0
%                     if isequal(davb,round(davb))==0 || issorted(davb)==0 || all(rem(davb,2))==0
%                         error('Must supply n bands as integer odd values in ascending order');
%                     end 
%                 end
            case 'ignore NaNs'
                ig_nan=varargin{c*2};  
%             case 'detrend'
%                 de_trend=varargin{c*2};
			
            otherwise
                error('Invalid option')
        end
    end
end
%% Data checks

[m n]=size(u1);
if m < n ;u1=transpose(u1);[m n]=size(u1);end

[m1 n1]=size(v1);
if m1 < n1 ;v1=transpose(v1);[m1 n1]=size(v1);end

if m1~=m | n1~=n
	error('Supplied velocities are not the same length');
end


%% Computations
T=m*1./Fs;
for ii=1:n
	[Coxy(:,ii) fxy CLxy PHC(:,ii) MSC(:,ii) Qxy(:,ii)]=cospec_welchCB(u1(:,ii),v1(:,ii),Fs,tplot,...
        'ignore NaNs',ig_nan,'nblocks',nsp); % results for 1 sided
end

[Px f  CL]=spec_welchCB(u1,Fs,tplot,'nbre segments',nsp,'ignore NaNs',ig_nan);
[Py f  CL]=spec_welchCB(v1,Fs,tplot,'nbre segments',nsp,'ignore NaNs',ig_nan);


%% Eq 5.8.50a of Emery and Thomson mulitplied by 1/2 as their Eq 0.5*(Suu +Svv -2Quv) assumes 2sided spectra. 
% My scripts return 1sided which are twice the 2-sided values, hence the additional 0.5 factor to get the 2-sided values
% Acutally that won't work as I'll be missing the negative frequencies!!! 
%I think I just need to mirroir the freq fx  to deal with the problem
Gp=0.5*(Px+Py+2*Qxy); % ALl one-sided, so should use same eq as Emery
Gm=0.5*(Px+Py-2*Qxy);


r=(Gp-Gm)./(Gp+Gm); % rotary coefficient

Pxy=Coxy-1i*Qxy; 
ephi=0.5*atan(2*Pxy./(Px-Py)) ; % angle of ellipse, not sure how to actually compute this as Gxy has a complex and imaginary part....

% Trying to calc length of major/minor axis
Ap=sqrt(0.5*Gp*T); % This may be wrong by a factor... going back to 2 sided-values
Am=sqrt(0.5*Gm*T);

Lm=abs(Ap-Am); % minor axis
LM=Ap+Am;% major axis

ep=2*sqrt(Ap.*Am)./(Ap+Am); % any wron factors above does not affect this calc
%% Plotting
if tplot
	figure
	ax(1)=subplot(2,1,1);
	hp=loglog(fxy,Gp ); hold on;% positive
	hm=loglog(fxy,Gm,'--'); % negative
	xlabel('Frequency [Hz]');
	ylabel('(input units)/Hz')
	title('Rotary spectra for 1 timeseries');
   
	hl=legend([hp(1); hm(1)],'G^+','G^-');legend('boxoff');set(hl,'fontsize',8);

	ax(2)=subplot(2,1,2);
	semilogx(fxy,r);
	xlabel('Frequency [Hz]');
	ylabel('Rotary coefficient []');
	grid on
	title('+ counterclockwise, - clockwise ');
end
figure
semilogx(fxy,ephi*180/pi,'m*')
ylabel('Phase  in deg b/w east and north counter cw from east')
xlabel('f [Hz]')
grid on