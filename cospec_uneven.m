function [Coxy  fxy MSC Gxx PHC dof Qxy Gyy CL]=cospec_uneven(xdata,ydata,xtim,ytim,specdat,Fs,tplot)
% Required
%   xtim / ytim: tim vectors in days (datenum matlab format is fine) for xdata and ydata.
%   specdat: structure of nblocks, nbands and fbands (which should have the same length)
%Optional:
%    Fs: specify only if both data sets were sampled evenly but are gappy
%    due to masking of bad data. 
% Refs: Shultz and Stattegger 1997 "Spectral analysis of unevenly spaced data
%       sets". Scargle 1989 "Spectral analysis"
% Limitations:
%       It returns PSD for the individual ts G, but it non-dimensionlazises
%       it according to the calculated average Nyquist freq
%       Hence, for high level spectral fitting (e.g. dissipation, you
%       should specify Fs for gappy data (i.e. sampled evenly but masked data)
% It detrends the data using detrend.m

%% Options
if nargin<7
    tplot=1;
end
%if nargin<6
 %   Fs=0; % will need to be assigned, if a value is assigned here..
    %we are assuming that both datasets collected evenly but gappy (bad)
%end
mwindow='hanning';

 
%% Removal of NaNs data points
gdpts=find(~isnan(xdata));
xdata=xdata(gdpts);
xtim=xtim(gdpts);

gdpts=find(~isnan(ydata));
ydata=ydata(gdpts);
ytim=ytim(gdpts);

xdata=detrend(xdata);%-mean(xdata);
ydata=detrend(ydata);%-mean(ydata);

%% Checks

Nx=length(xdata); 
if length(xtim)~=Nx
    error('both xtim and xdata should have same dimensions');
end

Ny=length(ydata); 
if length(ytim)~=Ny
    error('both ytim and ydata should have same dimensions');
end

%% Computations
dtx=mean(diff(xtim))*24*3600;
dty=mean(diff(ytim))*24*3600;

Tx=(xtim(end)-xtim(1))*24*3600;
Ty=(ytim(end)-ytim(1))*24*3600;

Fnx=1./(2*dtx); % Avg nyquist
Fny=1./(2*dty);

Nsx=floor(2*Nx./(specdat.nblocks+1)); % for segment, equals Nx if nsp=1
Nsy=floor(2*Ny./(specdat.nblocks+1));

dfx=1./(Nsx*dtx);% fundamental (lowest) freq 1/T 
dfy=1./(Nsy*dty);

%% Computatuins
df=max([dfx dfy]); % Eq 8 SS 1997
if Fs==0
   % relevant is truly using unevenly sampled data (and not evenly smapled with "bad" discarded data"
    dtxy=max([dtx dty]); % Eq. 7 SS 1997
    Fnxy=1./(2*dtxy);
else
    Fnxy=Fs/2; % nyquist
    dtxy=1./Fs;
end

tau0=(1./Nx)*sum(xtim)-(1./Ny)*sum(ytim); %Eq 13. SS 1997 (if Fs was speicified, and data sets aligned, this should be 0

disp([dtxy 1./(df*sqrt(Nsx*Nsy))])
%% Lomb-scargle dft estimate for each ts
%Fsxy=2*Fnxy;% sampling freq
fxy = transpose(df:df:Fnxy); 
w=2*pi*fxy;
    
[X1 Gxx scx]=lsft(w,xdata,xtim*24*3600,tau0,'nblocks',specdat.nblocks);
Gxx=Gxx./(Nsx*df);

[Y1 Gyy scy]=lsft(w,ydata,ytim*24*3600,0,'nblocks',specdat.nblocks); %
Gyy=Gyy./(Nsy*df);% scaled
    
Gtmp=(X1.*conj(Y1));
Gxy=2*sqrt(scx*scy)*sum(Gtmp,2)/(specdat.nblocks*df*sqrt(Nsx*Nsy));
    
[CL dof] = CL_cb(0.95,specdat.nblocks,'window',mwindow,'coherence',1);

%%
%G=[Gxx Gyy];
Coxy=real(Gxy);
Qxy = imag(Gxy);
%disp('must correct bias in coherence p933 in S&S1997')
MSC=(Coxy.^2+Qxy.^2)./(Gxx.*Gyy); % coherence
PHC=angle(Gxy)*180./pi;

if abs(tau0)>0.01*dtxy
    disp('aligning phase. Eq14 S&S1997')
    disp(tau0)
    for ii=1:length(PHC)
        tmpang=(PHC(ii)+tau0*fxy(ii)*360)*pi/180; % in rad
        % To ensure withing -180 to 180o
        [x y]=pol2cart(tmpang,1);
        nPHC(ii)=atan2(y,x)*180/pi; 
    end
    PHC=nPHC;
end

%% Now band-averaging desired outputs
ffields={'Coxy', 'MSC', 'Gxx', 'PHC', 'Qxy', 'Gyy'}; %'fxy' dof df
if specdat.nbands~=0
    for jj=1:length(ffields)
         eval(sprintf(strcat('dat=%s;'),char(ffields(jj))));
        [ndat tfxy tCL tdof]= bandavg(dat,fxy,specdat.nbands,specdat.fbands,CL,dof,specdat.nblocks,1,mwindow);
        eval(sprintf(strcat('%s=ndat;'),char(ffields(jj))));
    end
    fxy=tfxy;
    CL=tCL;
    dof=tdof;
end
warning('CL code is wrong')
[CL dof] = CL_cb(0.95,specdat.nblocks.*specdat.nbands,'window',mwindow,'coherence',1);
%% Plot
if tplot
    colsym={[0 0 0],[0 0.8 0.4]};
    figure 
    ax(1)=subplot(2,1,1);

    [ax,hMS,hPh] = plotxxm(fxy,MSC,fxy,PHC,'ylabels',{'Magnitude-Squared','Phase (degree)'},'color',colsym);    
    axpos=get(ax(1),'position');
    axpos(4)=0.95*axpos(4);    
    set(ax,'position',axpos)
      %  title(['MAGNITUDE-SQUADED  & PHASE  COHERENCE for ',legendlab{1},'\cdot',legendlab{2}]);

    set(hPh,'marker','*','linestyle','none');
    set(ax,'Xscale','Log','Xcolor','k');   	
    set(ax(1),'Ycolor',get(hMS,'color'),'ylim',[0 1],'ytick',0:0.25:1,'xgrid','on');   	
    set(ax(2),'Ycolor',get(hPh,'color'),'ylim',[-180 180],'ytick',-180:90:180,...
                'xticklabel',[],'Yticklabel',['-180';' -90';'   0';' +90';'+180']);


            %Co spectra
    ax(2)=subplot(2,1,2);
    if size(fxy)~=size(Coxy)
        fxy=fxy';
    end
    semilogx(fxy,fxy.*Coxy,'color',[0.7 0 0.85]);hold on;
     xlabel('Frequency [Hz]');ylabel('f * Co-PSD');
            title([int2str(specdat.nblocks) ' segments overlapping at 50% (hanning)' ]);

    xli=get(ax(2),'xlim');
    plot(xli,0*[1 1],'k--');

    set(ax,'xgrid','on','xlim',xli);

    %Confidence level on Coherency
     hCL= line(fxy,CL*ones(size(fxy)),'color',[0.7 0 0.85],'linestyle','--','Parent',ax(1));
end