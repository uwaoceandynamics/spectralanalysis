function [wbin Pbin CL]=binspectra(w,Pww,nsp,davb,dfb,tplot,mwindow)
% Bin a spectra already created, returns results in same units as input
% Probably written to deal with unevenly spaced spectra "after the fact"
% extra averaging given how slow it's to run
% Required inputs
%   w: freq 
%   Pww: power density 
%   nsp=number of bands and bins used intially (1= raw periodogram)
%  davb=number of points to average in a band
%   dfb= freq to start the band avg with davb (hence
%   length(davb)-length(dfb)
%Outputs:
%   wbin: Binned freq in rad/s
%   Pbin: Binned P in rad/s
%   CL: confidence interval
%% USer defined and data checks

pc=0.95; % 95% confidence interval
if ~isequal(length(davb),length(dfb))
    error('Both davb and dfb have to have the same lengths')
end
if nargin<6
    mwindow='hanning';
end

%% Calcs
[CL v] = CL_cb(pc,nsp,'window',mwindow);

% Let's improve accuracy at higher f by band averaging

    % Initialize beginning of interval
    cc=sum(w<=dfb(1));
    newPww=Pww(1:cc,:);
    newfw=w(1:cc);
	newCL=repmat(CL,[cc 1]);
	newdof(1:cc)=v;
    
    for ii=1:length(dfb)
        [binP,binw]=binavg_CB(Pww,w,davb(ii),'mean');
        [binCL vtmp]= CL_cb(pc,davb(ii)*nsp,'window',mwindow); % improving the estimate by band averaging (increase degrees of freedom)
        if ii==length(dfb)
        	ind=find(binw>dfb(ii));
       	else
            ind=find(binw>dfb(ii) & binw<=dfb(ii+1));
        end
        
        newPww(cc+1:cc+length(ind),:)=binP(ind,:);
        newfw(cc+1:cc+length(ind))=binw(ind);
		newCL(cc+1:cc+length(ind),:)=repmat(binCL,[length(ind) 1]);
        newdof(cc+1:cc+length(ind))=vtmp;
		cc=cc+length(ind);
    end
 %%   
 Pbin=newPww; wbin=newfw; CL=newCL;
 dof=newdof;
 clear new* dfb bin* ind L;

%% Optional plot
n=min(size(Pbin));
if tplot
    figure;
    if n>3
        set(gcf,'DefaultAxesColorOrder',jet(n));
    else
         set(gcf,'DefaultAxesColorOrder',colormap_CB(n,'gray'));
    end
    loglog(wbin,Pbin);hold on;
    xli=get(gca,'xlim');
    yli=get(gca,'ylim');
    CLp=CL*5*yli(1);
    if length(CL(:,1))>1
        ii=2;
    else
        ii=1;
    end
    
    if all([any(CLp(ii:end,1)~=CLp(ii,1)) davb])
       plot(wbin,CLp(:,[1;3]),'k-');
       text(wbin(2),CLp(1,3),'95%','verticalalignment','bottom','fontsize',8);
    else
        errorbar(wbin(2),CLp(ii,2),CLp(ii,1)-CLp(ii,2),CLp(ii,2)-CLp(ii,3),'ko','markersize',2);
        text(1.1*wbin(2),CLp(ii,2),'95%','verticalalignment','bottom','fontsize',8);
    end

    set(gca,'ylim',yli,'xlim',xli);
end