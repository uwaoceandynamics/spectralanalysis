function [hb, ht]=plotspec_errorbar(CL,fw,yloc)
% Plotting errorbars for spec_welchCB using the fw, CL output from spec_welchCB
% Exists mainly when adding multiple spectra created from different
% "routines"
if  nargin<3
    yli=get(gca,'ylim');
    yloc=yli(2)./10;
end
  
CLp=CL*yloc;
if length(CL(:,1))>1
    ii=2;
else
    ii=1;
end


if any(CLp(ii:end,1)~=CLp(ii,1))
   hb=plot(fw,CLp(:,[1;3]),'k-');
   ht=text(fw(2),CLp(1,3),'95%','verticalalignment','bottom','fontsize',8);
else
    hb=errorbar(fw(2),CLp(ii,2),CLp(ii,1)-CLp(ii,2),CLp(ii,2)-CLp(ii,3),'ko','markersize',2);
    ht=text(1.1*fw(2),CLp(ii,2),'95%','verticalalignment','bottom','fontsize',8);
end