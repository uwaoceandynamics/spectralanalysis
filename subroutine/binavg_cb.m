function [newdat,newx]=binavg_cb(data,x,Ne,option,gdratio)
% Bin averaging according to a certain number of points in each bin. Designed for equally spaced data sets although it will compute the bins for uneven ones too
% Required Inputs:
%	data: the data set that bin averaging is desired, if a matrx is supplied, bin averaging occurs along the longest dimension (ie if more rows, then binavg is done along each column)
%	x: independant vector depths, time, frequency of the data
% 	Ne: Number of points in each bin s
%	option: 'mean','max','std','min'  depending if we want mean, max, std, min of each bin
%Optional inputs
%	gdratio: Number of data points within each bin that must be present to  yield a mean , max or min value for the bin. By default gdratio=1, i.e. 100% of the data must be present
%		You cannot use this function with 'sum' if not all the data is present in the interval, it will NOT return a sum
%Output
%	data set representing the mean , max, min or sum for each bins
% Improvements
%	Enable the input of a vector of bins, namely to enable bining data and creating an evenly spaced data from uneven data set. 
%	The current script will bin a number of specified points Ne together creating an unevenly spaced data set if an uneven one is provided
%------------------------------------------------------------

if nargin <5
	gdratio=1; % do calcs along each column
end


[m n]=size(data);
if n>m
    data=transpose(data);
    [m n]=size(data);
	trp=true;
else 
	trp=false;
end

newdat(1:floor(m/Ne),n)=NaN; % initialising for speed
newx(1:floor(m/Ne))=NaN; % initialising for speed

for jj=1:n
	for ii=1:floor(m/Ne)
		ind=[(ii-1)*Ne+1:(ii-1)*Ne+Ne];
		if sum(isfinite(data(ind,jj)))>=gdratio*Ne  
            switch option
	   		case 'mean'
				newdat(ii,jj)=nanmean(data(ind,jj)); % the interval mean
	   		case 'max'
				newdat(ii,jj)=max(data(ind,jj)); % the interval max
	   		case 'min'
				newdat(ii,jj)=min(data(ind,jj)); % the interval min
	   		case 'sum'
				newdat(ii,jj)=sum(data(ind,jj)); % the interval sum, will retrun NaN if 1 data point is missing
			otherwise
				error('Invalid option')
            end    
        end
		newx(ii)=nanmean(x(ind));
    end
end

if trp
	newdat=transpose(newdat);
end
%size(newx)
%size(x)
end