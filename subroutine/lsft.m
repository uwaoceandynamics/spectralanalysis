function [X P scalfac]=lsft(w,data,t,tau0,varargin)
% Lombscargle unevenly spaced discrete fourier transform X at frequencies w
%  See spec_uneven to see how it's dimensionalized  back into  Hz
% t: must be supplied in seconds (matlab dates*24*3600s)

%% Data checks
N=length(data);
if ~isequal(length(t),N)
    error('data and t should ahve same lengths');
end
%% Options
scalfac=[];

nsp=1;
if ~isempty(varargin)
    for c=1:floor(length(varargin)/2)
        switch varargin{2*c-1}
        %case 'hann' % use hanning window with 50% overlap
          %  hanwin=true;
         case 'nblocks'
			nsp=varargin{2*c}; % nbre of segments to use in windowing
           
         otherwise
			error('You supplied wrong option');
        end
    end
end


%% Data Compute

Ne=floor(N/(nsp+1));
if nsp>1
    hw= hann(2*Ne);     
else
    hw=ones([2*Ne 1]); % no window raw
end
scalfac=length(hw)/sum(hw.^2); %8/3;%Use constant
[mw nw]=size(w);

for jj=1:nsp
        ind=(jj-1)*Ne+1:Ne*(jj+1);
		t_ens=t(ind);
        if size(data(ind))~=size(hw)
            data_ens=data(ind).*hw';
        else
            data_ens=data(ind).*hw;
        end
        
        [mt nt]=size(t_ens);
        if mw==1 
            w=w';
        end
        
        if mt~=1
            t_ens=t_ens';
        end
        
		tau = atan2(sum(sin(2*w*t_ens),2),sum(cos(2*w*t_ens),2))./(2*w);
		
		%spectral power
		cterm = cos(w*t_ens - repmat(w.*tau,1,2*Ne));
		sterm = sin(w*t_ens - repmat(w.*tau,1,2*Ne));
        Fo=sqrt(1/2)*exp(-1i*w.*(tau0-tau)); %vector 
        A=(sum(cterm.^2,2)).^-0.5; %Eq II.3 Scargle 1989
        B=(sum(sterm.^2,2)).^-0.5; %Eq II.3 Scargle 1989
        tmpc=cterm*diag(data_ens); tmps=sterm*diag(data_ens);
    
        for ii=1:length(w) % at each freq       
            P(ii,jj)=Fo(ii)*sum(A(ii)*tmpc(ii,:)+1i*B(ii)*tmps(ii,:)); % really dft X     
        end        
end

X=P;%mean(P,2);
P=2*scalfac*sum(abs(X).^2,2)./nsp;%raw 1-sided spectra for each segment??
