function [newPxx newfw newCL newdof]= bandavg(Pxx,fw,davb,dfb,CL,v,nsp,tcoh,mwindow)
% davb: # of bands to average (odd number)
% dfb: frequency in same units as fw to bandavg results
% I essentially took stuff out spec_WelchCB to run in standalone mode for other spectral functions
% tcoh=1/0 is band averaging cospectral/coherence results. THe output newCL
% is for the squared coherency. 
pc=0.95; % could be optional
if nargin<9
    mwindow='hanning'; % need to improve script to grab window
end
%%

dof(1:length(fw))=v;
% Let's improve accuracy at higher f by band averaging
if davb~=0
    % Initialize beginning of interval
    cc=sum(fw<=dfb(1));
    if cc>0
     
    newPxx=Pxx(1:cc,:);
    newfw=fw(1:cc);
	newCL=repmat(CL,[cc 1]);
	newdof(1:cc)=v;
    end
    
    for ii=1:length(dfb)
        [binP,binfw]=binavg_cb(Pxx,fw,davb(ii),'mean');
        [binCL vtmp]= CL_cb(pc,davb(ii)*nsp,'window',mwindow,'coherence',tcoh); % improving the estimate by band averaging (increase degrees of freedom)
        if ii==length(dfb)
        	ind=find(binfw>dfb(ii));
       	else
            ind=find(binfw>dfb(ii) & binfw<=dfb(ii+1));
        end
        
        newPxx(cc+1:cc+length(ind),:)=binP(ind,:);
        newfw(cc+1:cc+length(ind))=binfw(ind);
        if tcoh
		newCL(cc+1:cc+length(ind),1)=repmat(binCL,[length(ind) 1]);
        
        else
        newCL(cc+1:cc+length(ind),1:3)=repmat(binCL,[length(ind) 1]);
            
        end
        newdof(cc+1:cc+length(ind))=vtmp;
		cc=cc+length(ind);
    end
    
    %Pxx=newPxx; fw=newfw; CL=newCL;
    %dof=newdof;
    %clear new* dfb bin* ind L;
end