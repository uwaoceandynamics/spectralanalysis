function [CL v] = CL_cb(P,nsp,varargin)
% Confidence levels [CL] for Spectral Analaysis loglog plots using methodology described 
%		in Data Analysis Methods in Physical Oceanography (Emery and Thomson)
% Can also optionnally provide CL for coherence plots
%Required inputs
%	P=desired confidence interval as a fraction (0.95 for 95%)
%	nsp= the number of segments used for ensemble averaging OR the number of frequency bands averaged . The degrees of freedom =2*nsp if no window are used
% Optional argument
%	window: string stating the type of window used ("hanning", "hamming","rect") in order to correct the degrees of freedom. If no window is supplied, it assumes 'rect'
%	'coherence', 1/0 whether you want the confidence levels for coherence plots to be outputed in CL
% Outputs: 
%	CL=[CLmin CLmed CLmax]: minimum, median (CDF=50%), maximum value of the confidence interval for spectral estimates
%			Multiply these three values by the y value where you want the error to appear
%			Top error bar should longer than the bottom. At high DOF, unnoticeable,  the chi square prob distribution becomes more symmetric at higher degrees of freedom 
%	CL=level where coherence  value is significant if the 'coherence' option is selected
%
%Improvements and Limitations
%	Table 5.6.4 yields the degrees of freedom based on the window type.
%	Section on confidence levels for Coherence p.488 Emery & Thomson  is somewhat inconsistent. 
%		Their example would imply that the degrees of freedom in Table 5.6.4 would be twice as high. 
%		Applying 1 Hamming window to entire timeserie should yield DOF=2.51*N/M =5.032 rather than their stated EDOF=5.032 (DOF=10.06). 
%		Keep in mind that if their result was correct, the effect of applying a window  would give DOF=10.06 comapred to  DOF=2. That's too much improvement.
%		If I'm wrong, my script is effectively more conservative
%Uses:
%	It requires statistical toolbox in Matlab (chi2inv) although Octave does have this command.
%		Bendat and Piersol does have the Chi table that could be coded into this script (see OpClevel). 
% Comments on math if wanting to add more windows
%	The factors to correct for windowing are assumed constant but are in fact depend slightly on the number of samples. Although, in terms of changing the error bar, it's really not worth the effort
% 	They can be calculated as L/sum(hw.^2)
%		 	 where hw= are the window weights (as returned from hann(L)) and L is the number of points in each segment. 
%		In general if there are more than 100pts, the factor calculated above are within less than 1% of the constant that I'm using. 
%		For L=200, it's less than 0.5% etc..
%	If you plan on coding another window type, just calculate the factor using L=10000 (large #), then compute the window weights hw,  fact=L/sum(hw.^2)
%CBluteau 2009
%=================================================================================================
mwindow='hanning'; % equivalent to the use of no windows
ovlap=0.5;
tdof=false;
if ~isempty(varargin)
    for c=1:floor(length(varargin)/2)
        switch varargin{2*c-1}
        case 'window'
            mwindow=varargin{2*c};
			if ischar(mwindow)==0
				error('The supplied window should be a string');
			end
        case 'ovlap'
			ovlap=varargin{2*c};
			if ovlap>1 || ovlap<0
				error('the supplied ovlap must be a value b/w 0 and 1. For 50% overlap of windows ovlap=0.5'); 
			end
		case 'coherence'
			tdof=varargin{2*c}; % logical 1/0 whether you want to return coherence squared confidence levels (Eq 5.8.24 Emery & Thomson)
		otherwise
			error('Only ovlap and window are possible optional input arguments');
        end
    end
end

if P>=1 && P<=0
	error('P must be a value between 0 and 1 representing the desire confidence level. 0.95 is a good value');
end

switch mwindow
	case 'rect'
		v=nsp*2; % DOF
	case 'hanning' %factors are Priestly 1981 or table 5.6.4 Emery and Thomson EDOF=fact*N/M where M=walf width of window, N=length data. Translates to N/M=nsp+1
		if ovlap==0.5
			v=(8/3)*(nsp+1); % based on 50% over-lap. 8/3 factor is accurate to ~0.8% if number of points in each segment (L in spec_welch) is greater than 100. 
		else
			disp('Using a hanning window WITHOUT a 50% overlap, reverting to more conservative default degrees of freedom, uncorrected for windowing')
		end
	case 'hamming'
		if ovlap==0.5
			v=(2.5164)*(nsp+1); % based on 50% over-lap
		else
			disp('Using a hanning window WITHOUT a 50% overlap, reverting to more conservative default degrees of freedom, uncorrected for windowing')
		end	
	otherwise
		error('Supplied window name not currently supported. Its an optional argument, by default it will not correct the supplied degrees of freedom')
end


%% Computations============================================================================
alph = (1-P)/2;
if tdof
	CL=1-alph.^(2/(v-2));
else
	CLmax=(v./chi2inv(alph,v)); % 
	CLmin = (v./chi2inv(1-alph,v));
	CLmed= (v./chi2inv(0.5,v));
	CL=[CLmin CLmed CLmax];
end
