function [Pxx, fw,  CL, dof, hp]=spec_welchCB(data,Fs,tplot,varargin)
%function [Pxx fw  CL dof hp]=spec_welchCB(data,Fs,tplot,varargin)
% Spectral estimates using pwelch method hamming window, 50% overlap. It makes use of pwelch.m
%   	It can also perform band averaging from specified frequency and number of bands to average. 
%	It will also put 95% confidence limits on plot
%
% Required inputs:
%   	data: the data on which to do the spectral estimates. Can be a
%   	matrix. If a matrix supplied, then the psd estimates will be done along the longest dimensions
%   	Fs: sampling frequency
%   	tplot: 1 or true if  plot desired, 0 if no plot desired
% Optional Inputs:
%      'conf level': confidence interval for limits (pc). 0.95 (95%) is the
%               default
%    	'nbre segments': number of windows to use in pwelch. By default it is 8.
%    	'fband avg': frequency from which we should band average (dfb). 
%                   By default, no band averaging is undertaken
%    	'n bands': number of bands to use for band averaging 
%                   By default it should have 2 int values if no 'fband avg' are supplied. 
%                   Otherwise it should have the same length as 'fband_avg'
%                   Need to be monotonically increasing to improve accuracy at higher frequencies
%                   If no band averaging is desired supply a value of 0
%       'detrend': if detrending of dat is desired. By default there is no
%                   linear detrending, just mean removed
%	'window': string specifying the window type 'hann' (default),or  are supported
%	'hamm' 
% Outputs:
%   Pxx and fw are in Hz, not rad/s (one-sided spectra)
%       hp= handle of spectra plots, empty if tplot=0;
%       dof: degrees of freedom of the spectra (used for spectral fitting
%           algorithms)
% Uses:
%       Signal Processing toolbox
%       Statistics toolbox (just for error bars, I've coded the chi distribution table too)		
%-------------------------------------------------------------------------
%% Defaults for options
pc=0.95; % confidence level 95%
dfb=0; % desired frequency intervals to band average
davb=0; % corresponding # of bands to average of original record
nsp=8;
ig_nan=0;
de_trend=0;
mwindow='hanning';
trad=false;
hfig=false;
yli=[];
hp=[];
if ~isempty(varargin)
    for c=1:floor(length(varargin)/2)
        switch varargin{2*c-1}
            case  'conf level'
                pc=varargin{c*2};
            case {'nbre segments','nblocks'}
                nsp=varargin{c*2};
                
            case {'fband avg','fbands'}
                dfb=varargin{c*2};
                if issorted(dfb)==0
                    error('Must supply frequencies in ascending order');
                end
            case {'n bands','nbands'}
                davb=varargin{c*2};  
                if davb~=0
                    if isequal(davb,round(davb))==0 || issorted(davb)==0 || all(rem(davb,2))==0
                        error('Must supply n bands as integer odd values in ascending order');
                    end 
                end
            case 'ignore NaNs'
                ig_nan=varargin{c*2}; % replaces with white noise with amplitude equal to sqrt(variance of signal)
            %case 'length of window'   To force the number of points used
            case 'ylim'
                yli=varargin{2*c};
            case 'rad' % work in radians
                trad=varargin{2*c}; % logical 1/0
			case 'window'
				mwindow=varargin{2*c}; % string for window type
				if ~ischar(mwindow)
					error('You must supply a string for the window type');
				end
			case 'hold on'
				hfig=varargin{2*c}; % 1/0 whether to plot in current figure or to a new fig. By default a new figure is opened
            otherwise
                error('Invalid option')
        end
    end
end

if davb~=0
    if isequal(length(davb),length(dfb))==0
        error('the frequencies to start band avg must be same length as the desired nbre of bands for averaging');
    end
end

[m n]=size(data);
if n>m
    data=transpose(data);
    [m n]=size(data);
end

if ig_nan % replaces NaNs with white noise with same variance as data
    %for data set with only a few bad data points. 
    warning('SHould really used unevenly spaced techniques, but this is how Stanford crew do this');
    %tmpstd=nanstd(data);
   for jj=1:n
       ind=find(isnan(data(:,jj)));
       tmpmean=nanmean(data(:,jj));
      % bind=find(~isnan(data(:,jj)));tmpA=std(data(bind,jj));
       tmpA=nanstd(data(:,jj));
       %tmpA=tmpA.^2; % this is the variance, the area under the spectra curve!
       data(ind,jj)=tmpA.*randn([length(ind) 1])+tmpmean;%data(ind,:);
   end
   %[m n]=size(data);
end


%% Main Script
L=floor(2*m/(nsp+1)); % using 50% overlap
% Calculations for window weights for spectral estimate
switch mwindow
	case {'Hann','hann','hanning','Hanning'}	
		hw=hann(L);
		mwindow='hanning';
	case {'Hamm','hamm','hamming','Hamming'}
		hw=L; % default in matlab is to use hamming
		mwindow='hamming';
	otherwise
		error('This window function is not supported yet, just code in the respective window wt');
end

% Spectral of original data
for jj=1:n % for each data set provided
  %  y=data(:,jj);
    y=detrend(data(:,jj));
    [Pxx(:,jj),fw] = pwelch(y,hw,[],[],Fs,'onesided'); 
end
clear yt data;

[CL v] = CL_cb(pc,nsp,'window',mwindow);
dof(1:length(fw))=v;
% Let's improve accuracy at higher f by band averaging
if davb~=0
    % Initialize beginning of interval
    cc=sum(fw<=dfb(1));
    newPxx=Pxx(1:cc,:);
    newfw=fw(1:cc);
	newCL=repmat(CL,[cc 1]);
	newdof(1:cc)=v;
    
    for ii=1:length(dfb)
        [binP,binfw]=binavg_cb(Pxx,fw,davb(ii),'mean');
        [binCL vtmp]= CL_cb(pc,davb(ii)*nsp,'window',mwindow); % improving the estimate by band averaging (increase degrees of freedom)
        if ii==length(dfb)
        	ind=find(binfw>dfb(ii));
       	else
            ind=find(binfw>dfb(ii) & binfw<=dfb(ii+1));
        end
        
        newPxx(cc+1:cc+length(ind),:)=binP(ind,:);
        newfw(cc+1:cc+length(ind))=binfw(ind);
		newCL(cc+1:cc+length(ind),:)=repmat(binCL,[length(ind) 1]);
        newdof(cc+1:cc+length(ind))=vtmp;
		cc=cc+length(ind);
    end
    
    Pxx=newPxx; fw=newfw; CL=newCL;
    dof=newdof;
    clear new* dfb bin* ind L;
end

%% Convert into radians (optional)
if trad
    Pxx=Pxx./(2*pi);
    fw=fw*2*pi;
end

%% Plotting===================================================================================
if tplot
	if ~hfig
        figure 
        if n==4
             colsym=[ 0    0.25    0.8; 
                    0 0.8 0.25;
                    1 0.5 0;
                    0.8 0.1 0.1];                  
        else
            if n<=3
                colsym=lines(n);
            else
                colsym=jet(n);
            end
        end
        
        if n>3
        set(gcf,'DefaultAxesColorOrder',colsym);
        end

    end

    hp=loglog(fw,Pxx);hold on;
    
    xli=get(gca,'xlim');
    if isempty(yli)
        yli=get(gca,'ylim');
    end
    
 
    plotspec_errorbar(CL,fw,yli(2)./10);
%     CLp=CL*yli(2)/10;
%     if length(CL(:,1))>1
%         ii=2;
%     else
%         ii=1;
%     end
%     
% %disp(CLp)
%     if all([any(CLp(ii:end,1)~=CLp(ii,1)) davb])
%        plot(fw,CLp(:,[1;3]),'k-');
%        text(fw(2),CLp(1,3),'95%','verticalalignment','bottom','fontsize',8);
%     else
%         errorbar(fw(2),CLp(ii,2),CLp(ii,1)-CLp(ii,2),CLp(ii,2)-CLp(ii,3),'ko','markersize',2);
%         text(1.1*fw(2),CLp(ii,2),'95%','verticalalignment','bottom','fontsize',8);
%     end
%     
% 
%    
    if trad
        xlabel('Frequency [rad s^{-1}]');
    else
        xlabel('Frequency [Hz]');
    end
    ylabel('PSD');
    title([int2str(nsp) ' segments overlapping at 50% (', mwindow,')' ]);
    set(gca,'ylim',yli,'xlim',xli);

end

%%
[m n]=size(fw);
if m<n
    fw=transpose(fw);
end
if nargin <=1
    ntmp=[Pxx fw  CL];
    clear Pxx;
    Pxx=ntmp;
end