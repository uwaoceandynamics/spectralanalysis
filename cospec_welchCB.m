function [Coxy, fw, CL, PHC, MSC, Qxy, Pxx, Pyy]=cospec_welchCB(xdata,ydata,Fs,tplot,varargin)
% Syntax: [Coxy, fw, CL, PHC, MSC,
% Qxy]=cospec_welchCB(xdata,ydata,Fs,tplot)
% Cospectral estimates using pwelch method hamming window, 50% overlap. It makes use of cpsd.m
%   	It can also perform band averaging from specified frequency and number of bands to average. 
%	It need to also put 95% confidence limits on plot
% Required inputs:
%   	xdata and ydata: two data sets to calculate cospectra estimates. Each can be a matrices. 
%			If a matrix, then psd estimates  done along the longest dimension
%   	Fs: sampling frequency
%   	tplot: 1/0 if  plot desired or not
% Optional Inputs:
%      'conf level': confidence interval for limits (pc). 0.95 (95%) is the
%               default
%    	'nblocks': number of windows to use in pwelch. By default it is 8.
%    	'fband': frequency from which we should band average (dfb). 
%                   By default no band avg is done
%    	'nbands': number of bands to use for band averaging 
%                   By default it should have 2 int values if no 'fband' are supplied. 
%                   Otherwise it should have the same length as 'fband'
%                   Need to be monotonically increasing to improve accuracy at higher frequencies
%                   If no band averaging is desired supply a value of 0
%		'window': string specifying the window type 'hann' (default) or 'hamm'
%					Can easily code in other windows using built-in matlab fcts
% Outputs:
%		'Coxy' : cospectra which is the real part of the cross-sepctra density (Gxy) from Bendat and Piersol
% Improvements & Limitations
%       Data are detrended using detrend.m
%Understanding the calculated phase
%   If PHASE IS POSITIVE means that serie1 is +TETA from serie2;
%   If PHASE IS NEGATIVE means that serie1 is -TETA from serie2;
%
%   serie1 = cos(2*pi*x + TETA) equiv. w=cos(alpha) 
%   serie2 = cos(2*pi*x)        equiv. rho=sin(alpha) if TETA=+90
%       
%   For buoyancy fluxes (w*rho), the phase for linear internal waves should be +90deg. 
%   Hence, supply w as the xdata and rho as the ydata.
% USes:
%   Stats and signal processing toolboxes
%   CL_cb, binavg_CB, and plotxxm functions created/adapted by CBluteau
%-------------------------------------------------------------------------
%% Defaults for options
pc=0.95; % confidence level 95%
dfb=0; % desired frequency intervals to band average
davb=0; % corresponding # of bands to average of original record
nsp=8;


mwindow='hanning';
legendlab={'Series 1','Series 2'};
if ~isempty(varargin)
    for c=1:floor(length(varargin)/2)
        switch varargin{2*c-1}
            case  'conf level'
                pc=varargin{c*2};
            case 'nblocks'
                nsp=varargin{c*2};
            case 'fbands'
                dfb=varargin{c*2};
                if issorted(dfb)==0
                    error('Must supply frequencies in ascending order');
                end
            case 'nbands'
                davb=varargin{c*2};  
                if davb~=0
                    if isequal(davb,round(davb))==0 || issorted(davb)==0 || all(rem(davb,2))==0
                        error('Must supply n bands as integer odd values in ascending order');
                    end 
                end
%             case 'ignore NaNs'
%                 ig_nan=varargin{c*2};  
			
			case 'labels'
				legendlab=varargin{c*2};
			case 'window'
				mwindow=varargin{2*c}; % string for window type
				if ~ischar(mwindow)
					error('You must supply a string for the window type');
				end
            otherwise
                error('Invalid option')
        end
    end
end

if davb~=0
    if isequal(length(davb),length(dfb))==0
        error('the frequencies to start band avg must be same length as the desired nbre of bands for averaging');
    end
end


sfields={'xdata','ydata'};
% Transposing if needed
for kk=1:2
	%if kk==1;data=xdata;else;data=ydata;end
    eval(sprintf('data =%s;',char(sfields(kk))));
    
	[m n]=size(data);
	if n>m;data=transpose(data);[m n]=size(data);end	
	eval(sprintf('%s=data;',char(sfields(kk))));
end


    
if size(xdata)~=size(ydata)
	error('X and Y data sets are different sizes')
end
[m n]=size(xdata);

%% Main Script
L=floor(2*m/(nsp+1)); % using 50% overlap
% Calculations for window weights for spectral estimate
switch mwindow
	case {'Hann','hann','hanning','Hanning'}	
		hw=hann(L);
		mwindow='hanning';
	case {'Hamm','hamm','hamming','Hamming'}
		hw=L; % default in matlab is to use hamming, just need to specify length of window L
		mwindow='hamming';
	otherwise
		error('This window function is not supported yet, just code in the respective window wt');
end

% Spectral of original data
for jj=1:n % for each data set provided
     y=detrend(ydata(:,jj));
     x=detrend(xdata(:,jj)); 
    
    % [Pxx, fw,  CL, dof, hp]=spec_welchCB(data,Fs,tplot,varargin)
     %[Pxx(:,jj),fw] = pwelch(y,hw,[],[],Fs,'onesided'); % from spec_welchCB 
    [Pxx(:,jj),ftmp] = pwelch(x,hw,[],[],Fs,'onesided'); %PSD
	[Pyy(:,jj),ftmp] = pwelch(y,hw,[],[],Fs,'onesided'); 
    %Can you supply matices to pwelch, etc
	[Pxy(:,jj),fw] = cpsd(x,y,hw,[],[],Fs,'onesided'); % Pxy=Cxy- iQxy (??? Coincident and Quadrature)
	% Using the raw calcs in order to allow for band averaging which must applied on Pxy, Pxx, Pyy and NOT MSC
	%[MSC(:,jj),ftmp] = mscohere(x,y,hw,[],[],Fs); %OrMSC=(real(Pxy).^2+imag(Pxy).^2)./(Pxx.*Pyy) =  Cxy^2 + Qxy^2 /(Pxx.*Pyy) 
    if any(fw-ftmp); error('Problem with ftmp and fw, they should be the same');end
end


CI = CL_cb(pc,nsp,'window',mwindow,'coherence',1);%Conf interval
CL=repmat(CI,[length(fw) 1]);

%data=[real(Pxy) angle(Pxy)*180./pi MSC imag(Pxy)]; % [Cospectra Phase Coherence^2  Qxy]  ; Cospectra= Cxy in the Eq  Pxy=Cxy- iQxy ..imag(Pxy)=Qxy
data=[real(Pxy) angle(Pxy)*180./pi Pxx Pyy imag(Pxy)];
clear  xdata ydata xt yt ftmp; 
% Let's improve accuracy at higher f by band averaging
if davb>1
    % Initialize 
    cc=sum(fw<=dfb(1));
    newdat=data(1:cc,:);
    newfw=fw(1:cc);
	newCL=repmat(CI,[cc 1]);

    for ii=1:length(dfb)
        [bindat,binfw]=binavg_cb(data,fw,davb(ii),'mean');
        binCL = CL_cb(pc,davb(ii)*nsp,'window',mwindow,'coherence',1); % improving the estimate by band averaging (increase degrees of freedom)        
		if ii==length(dfb)
        	ind=find(binfw>dfb(ii));
       	else
            ind=find(binfw>dfb(ii) & binfw<=dfb(ii+1));
        end
        newdat(cc+1:cc+length(ind),:)=bindat(ind,:);
        newfw(cc+1:cc+length(ind))=binfw(ind);
		newCL(cc+1:cc+length(ind),:)=repmat(binCL,[length(ind) 1]);
		cc=cc+length(ind);
    end
    data=newdat;
    [mfw nfw]=size(newfw);
    if mfw<nfw
        fw=newfw';
    else
        fw=newfw;
    end
    CL=newCL;    
    clear  new* bin* dfb  ind L CI cc;  
end

Coxy=data(:,1:n);
PHC=data(:,n+1:2*n);
Pxx=data(:,2*n+1:3*n);
Pyy=data(:,3*n+1:4*n);

Qxy = data(:,4*n+1:5*n);
MSC=(Coxy.^2+Qxy.^2)./(Pxx.*Pyy); %data(:,2*n+1:3*n);

% Testing whether Rolf's and my definition were the same
%TMP=Pxy.*conj(Pxy);
%TMP2=(Coxy.^2+Qxy.^2);
 %% Plotting Cospectra======================================================================
if tplot
    figure
	
	subplot(2,1,1)
	[ax,hMS,hPh] = plotxxm(fw,MSC,fw,PHC,'ylabels',{'Magnitude-Squared','Phase (degree)'},'color',{[0.5 0 1],[0.1 0.7 0.1]});
    axpos=get(ax(1),'position');
    axpos(4)=0.95*axpos(4);
    set(ax,'position',axpos)
    title(['MAGNITUDE-SQUADED  & PHASE  COHERENCE for ',legendlab{1},'\cdot',legendlab{2}]);
		set(hPh,'marker','*','linestyle','none');
    	set(ax,'Xscale','Log','Xcolor','k');
    	set(ax(1),'Ycolor',get(hMS,'color'),'ylim',[0 1],'ytick',0:0.25:1,'xgrid','on');
    	set(ax(2),'Ycolor',get(hPh,'color'),'ylim',[-180 180],'ytick',-180:90:180,...
    		'xticklabel',[],'Yticklabel',['-180';' -90';'   0';' +90';'+180']);
    
        hold on;
	hCL= line(fw,CL,'color',[0 0.75 0.2],'linestyle','--','Parent',ax(1));
	    text(fw(2),CL(2),strcat(num2str(100*pc),'%'),'verticalalignment','bottom','fontsize',8,'Parent',ax(1));

    
	subplot(2,1,2)
	semilogx(fw,fw.*Coxy);hold on;
	    xli=get(gca,'xlim');yli=get(gca,'ylim');
    semilogx(xli,0*[1 1],'k--');
  	    xlabel('Frequency [Hz]');ylabel('f * Co-PSD');
	    title([int2str(nsp) ' segments overlapping at 50% (', mwindow,')' ]);
	    set(gca,'ylim',yli,'xlim',xli,'xgrid','on');
		set(ax(1:2),'xlim',xli);
end

%% Output
[m n]=size(fw);
if m<n
    fw=transpose(fw);
end
